# form-doc

## VUE 2

Компонент VUE позволяющий заполнять формы в виде документа, с последующим сохранением, печатью или выгрузки в PDF.

Vue component use for fill forms presents of document. Letter Save, Print or Exprt to PDF.

### Install

```
npm install form-doc
```

if not install sass-loader in project

```
npm install -D sass-loader@^10 sass
```

edit or create file vue.config 
```
module.exports = {
  assetsDir: 'static',
  runtimeCompiler: true
}
```

### Use

see: https://codesandbox.io/s/intelligent-browser-t0wwp?file=/src/components/HelloWorld.vue 

### HomePage

see: https://gitlab.com/komtek-front/form-doc 


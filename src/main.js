import Vue from 'vue'
import App from './App.vue'
import "./components";

import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-chalk/index.css'


Vue.config.productionTip = false

Vue.use(VueMoment, {moment,})
moment().utcOffset(0, true).format()

Vue.use(ElementUI, { locale })


new Vue({
  render: h => h(App),
  components: { App },
}).$mount('#app')

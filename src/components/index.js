import Vue from "vue";
import editDate from "./editDate.vue";
import editDiv from "./editDiv.vue";
import editSelect from "./editSelect.vue";
import editView from "./editView.vue";
import editTable from "./editTable.vue";
import formDoc from "./FormDoc.vue";
import formDocView from "./FormDocView.vue";
import editMultiSelect from "./editMultiSelect.vue";


const Components = {
  formDoc,
  formDocView,
  editDate,
  editDiv,
  editSelect,
  editView,
  editTable,
  editMultiSelect
};

Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name]);
});

export default Components;
